package com.example.android.courtcounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final int TEAM_A_SCORE = 0;
    private final int TEAM_B_SCORE = 1;

    private int[] score = {0, 0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize(null);
    }

    public void initialize(View view) {
        score[TEAM_A_SCORE] = 0;
        score[TEAM_B_SCORE] = 0;

        displayScore(findViewById(R.id.team_a), score[TEAM_A_SCORE]);
        displayScore(findViewById(R.id.team_b), score[TEAM_B_SCORE]);
    }

    public void displayScore(View v, int score){
        if (v == findViewById(R.id.team_a)) {
            this.score[TEAM_A_SCORE] += score;
            ((TextView)findViewById(R.id.team_a_score)).setText(
                    String.valueOf(this.score[TEAM_A_SCORE])
            );
        } else if (v == findViewById(R.id.team_b)) {
            this.score[TEAM_B_SCORE] += score;
            ((TextView)findViewById(R.id.team_b_score)).setText(
                    String.valueOf(this.score[TEAM_B_SCORE])
            );
        }
    }

    public void threePoints_onClick(View view){
        displayScore((View)view.getParent().getParent(), 3);
    }

    public void twoPoints_onClick(View view){
        displayScore((View)view.getParent().getParent(), 2);
    }

    public void freeThrow_onClick(View view){
        displayScore((View)view.getParent().getParent(), 1);
    }
}
