package com.example.android.sunshine.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    ArrayList<String> forecastEntry;
    String[] forecastEntryArray = {
            "Today - Sunny - 88/63",
            "Tomorrow - Foggy - 70/46",
            "Weds - Cloudy - 72/63",
            "Thurs - Rainy - 64/51",
            "Fri - Foggy - 70/46",
            "Sat - Sunny - 76/68",
            "Sun - Foggy - 70/46",
            "Mon - Cloudy - 72/63",
            "Tues - Rainy - 64/51",
            "Weds - Foggy - 70/46",
            "Thurs - Sunny - 76/68"
    };

    private final String WEATHER_APPID = "APPID=41af502a33105c68c9bea3c83710ffd3";
    private final String WEATHERAPI_FORECAST = "http://api.openweathermap.org/data/2.5/forecast/";
    private String weatherSearch = "daily?q=94043,us";
    private String weatherMode = "mode=json";
    private String weatherUnit = "units=metric";
    private String weatherDayCount = "cnt=7";

    public static String weatherUrl_string;
    URL weatherUrl;
    HttpURLConnection httpConnection;

    InputStream inputStream = null;
    BufferedReader bufferedReader = null;

    public MainActivityFragment() {
        weatherUrl_string =  WEATHERAPI_FORECAST
                + weatherSearch + "&"
                + weatherMode + "&"
                + weatherUnit + "&"
                + weatherDayCount + "&"
                + WEATHER_APPID;

        try {
            weatherUrl = new URL(weatherUrl_string);
            httpConnection = (HttpURLConnection)weatherUrl.openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.connect();
            inputStream = httpConnection.getInputStream();

            Log.i("URL LOADED:", weatherUrl_string);
        } catch (IOException e){
            Log.i("URL FAILED:", "Could not construct url");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        forecastEntry = new ArrayList<String>(Arrays.asList(forecastEntryArray));
        ArrayAdapter<String> foreCastAdapter = new ArrayAdapter<String>(
                getActivity(),
                R.layout.list_item_forecast,
                R.id.list_item_forecast_textview,
                forecastEntry);


        ListView listView = (ListView) rootView.findViewById(R.id.listview_forecast);
        listView.setAdapter(foreCastAdapter);

//        forecastEntry.add("Today - Sunny - 88/63");
//        forecastEntry.add("Today - Sunny - 88/63");

        return rootView;

    }
}
