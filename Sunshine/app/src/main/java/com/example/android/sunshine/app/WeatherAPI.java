package com.example.android.sunshine.app;

import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by xuthan on 21-Jan-16.
 */
public class WeatherAPI {
    private final String WEATHER_APPID = "APPID=41af502a33105c68c9bea3c83710ffd3";
    private final String WEATHERAPI_FORECAST = "http://api.openweathermap.org/data/2.5/forecast/";
    private String weatherSearch = "daily?q=94043,us";
    private String weatherMode = "mode=json";
    private String weatherUnit = "units=metric";
    private String weatherDayCount = "cnt=7";

    URL weatherURL = null;
    HttpURLConnection weatherConnection = null;


    public URL setWeatherURL(String url){
        try {
            weatherURL = new URL(url);
        } catch (MalformedURLException e) {
            Log.i("MalformedURLException", e.getMessage());
            return null;
        }
        return weatherURL;
    }

    public URL getWeatherURL(){
        return weatherURL;
    }

    public void setWeatherConnection() throws IOException{
        weatherConnection = (HttpURLConnection)weatherURL.openConnection();
        weatherConnection.setRequestMethod("GET");
        weatherConnection.connect();
    }

    public HttpURLConnection getWeatherConnection(){
        return weatherConnection;
    }
}
