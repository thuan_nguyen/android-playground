package com.example.android.intentionallybrokenforcoding;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    @Override
    public void onBackPressed(){
        initialize();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize(){
        ((ImageView) findViewById(R.id.android_cookie_image_view)).setImageResource(R.drawable.before_cookie);
        ((TextView)findViewById(R.id.status_text_view)).setText("I'm so hungry");
        ((Button)findViewById(R.id.eat_button)).setClickable(true);
    }
    /**
     * Called when the cookie should be eaten.
     */
    public void eatCookie(View view) {
        // TODO: Find a reference to the ImageView in the layout. Change the image.
//        ImageView cookieImage = (ImageView)findViewById(R.id.android_cookie_image_view);
//        cookieImage.setImageResource(R.drawable.after_cookie);

        ((ImageView) findViewById(R.id.android_cookie_image_view)).setImageResource(R.drawable.after_cookie);

        // TODO: Find a reference to the TextView in the layout. Change the text.
        ((TextView)findViewById(R.id.status_text_view)).setText("I'm so full");
        ((Button)findViewById(R.id.eat_button)).setClickable(false);
    }
}