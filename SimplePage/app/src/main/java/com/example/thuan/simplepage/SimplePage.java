package com.example.thuan.simplepage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SimplePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simple_page);
    }
}
