package com.example.android.justjava;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
    private static String message = "";

    private int numberOfCoffees;
    private final int MAX_COFFEES = 10;
    private final int MIN_COFFEES = 1;

    private final int priceOfCoffee = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        numberOfCoffees = 1;
        displayQuantity(numberOfCoffees);
//        displayTotalPrice(numberOfCoffees);
    }

    public void submitOrder(View view) {
//        displayTotalPrice(numberOfCoffees);

        TextView messageText = (TextView) findViewById(R.id.order_message);
        message = "";
        String name = ((EditText) findViewById(R.id.name_edit_view)).getText().toString();

//        if (quantity > 0) {
//            messageText.setTextColor(Color.rgb(0, 0, 0));
//            messageText.setTextSize(18);

        if (numberOfCoffees == 1) {
            message = numberOfCoffees
                    + " " + getResources().getString(R.string.cup).toLowerCase();
        } else {
            message = numberOfCoffees
                    + " " + getResources().getString(R.string.cups).toLowerCase();
        }

        message += (" " + getResources().getString(R.string.of_coffee).toLowerCase());

        boolean topping1Checked = ((CheckBox) findViewById(R.id.topping_checkbox_1)).isChecked();
        boolean topping2Checked = ((CheckBox) findViewById(R.id.topping_checkbox_2)).isChecked();

        if (topping1Checked) {
            message += " "
                    + (getResources().getString(R.string.with).toLowerCase()
                    + " "
                    + getResources().getString(R.string.whipped_cream).toLowerCase());
        }

        if (((CheckBox) findViewById(R.id.topping_checkbox_2)).isChecked()) {
            if (topping1Checked) {
                message += " "
                        + (getResources().getString(R.string.and).toLowerCase()
                        + " "
                        + getResources().getString(R.string.chocolate).toLowerCase());
            } else {
                message += " "
                        + (getResources().getString(R.string.with).toLowerCase()
                        + " "
                        + getResources().getString(R.string.chocolate).toLowerCase());
            }
        }

        String totalPrice = NumberFormat.getCurrencyInstance().format(
                calculateTotal(
                        topping1Checked,
                        topping2Checked
                )
        );

        message += (".\n\n" + getResources().getString(R.string.total) + ": " + totalPrice);
        message += ("\n" + getResources().getString(R.string.thank_you));

//        }
//
//        else {
//            message += "Come on, you can do better than that";
//            messageText.setTextSize(24);
//            messageText.setTextColor(Color.rgb(255, 0, 0));
//        }

        if (!name.isEmpty()) name = ", " + name;
        else Log.e("Missing name", "Name not provided by user");

        message += name + "!";
        displayMessage(message);
        ((ScrollView) findViewById(R.id.scroll_order_view)).fullScroll(View.FOCUS_DOWN);

        AlertDialog.Builder sendEmailDialog = new AlertDialog.Builder(this);

        sendEmailDialog
                .setMessage("Email transaction?")
                .setNegativeButton("No", null)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        emailTransaction(message);
                    }
                })
                .show();

    }

    public void increment(View view) {
        if (numberOfCoffees < MAX_COFFEES) {
            displayQuantity(++numberOfCoffees);
        } else {
            String coffeePlural = (
                    MAX_COFFEES == 1
                            ? getResources().getString(R.string.coffee).toLowerCase()
                            : getResources().getString(R.string.coffees).toLowerCase());

            Toast.makeText(
                    this,
                    getResources().getString(R.string.you_cannot_have_more_than)
                            + " " + MAX_COFFEES
                            + " " + coffeePlural,
                    Toast.LENGTH_LONG).show();
        }
    }

    public void decrement(View view) {
        if (numberOfCoffees > MIN_COFFEES) {
            displayQuantity(--numberOfCoffees);
        } else {
            String coffeePlural = (
                    MIN_COFFEES == 1
                            ? getResources().getString(R.string.coffee).toLowerCase()
                            : getResources().getString(R.string.coffees).toLowerCase());

            Toast.makeText(
                    this,
                    getResources().getString(R.string.you_cannot_have_less_than)
                            + " " + MIN_COFFEES
                            + " " + coffeePlural,
                    Toast.LENGTH_LONG).show();
        }
    }

    public void emailTransaction(String message) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Just Java transaction");
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        if (emailIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(emailIntent);
            Log.i("Intent sent", "Intent was sent");
        } else {
            Log.e("Intent problem", "Could not send email intent");
        }
    }

//    private void displayTotalPrice(int numberOfCoffees) {
//        TextView priceTextView = (TextView) findViewById(R.id.total_price_text);
//        String total = NumberFormat.getCurrencyInstance().format(calculateTotal(numberOfCoffees));
//        priceTextView.setText("Total: " + total);
//    }

    private void displayQuantity(int numberOfCoffees) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText(Integer.toString(numberOfCoffees));
    }

    private void displayMessage(String message) {
        TextView messageText = (TextView) findViewById(R.id.order_message);
        messageText.setText(message);
    }

    public int calculateTotal(boolean creamChecked, boolean chocolateChecked) {
        int priceOfCoffee = this.priceOfCoffee;
        if (creamChecked) ++priceOfCoffee;
        if (chocolateChecked) priceOfCoffee += 2;
        return numberOfCoffees * priceOfCoffee;
    }


}
